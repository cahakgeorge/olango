package com.laclicservices.olango.viewbinder;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laclicservices.olango.R;
import com.laclicservices.olango.viewbinder.model.LanguageListItem;
import com.laclicservices.olango.viewbinder.viewholder.LanguageListViewHolder;

import java.util.ArrayList;


//gotten from http://www.appifiedtech.net/2015/02/15/android-recyclerview-example/
public class LanguageListRecycler extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<LanguageListItem> languageList;

    public LanguageListRecycler(Context context, ArrayList<LanguageListItem> languageList) {
        this.mContext = context;
        this.languageList = languageList;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.languageList.size();
    }

    /**
     * This method creates different RecyclerView.ViewHolder objects based on the item view type.\
     *
     * @param viewGroup ViewGroup container for the item
     * @param viewType type of view to be inflated
     * @return viewHolder to be inflated
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View view = inflater.inflate(R.layout.choose_language_item, viewGroup, false);
        viewHolder = new LanguageListViewHolder(view, mContext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        LanguageListViewHolder vh1 = (LanguageListViewHolder) viewHolder;
        configureViewHolder1(vh1, position);
    }

    private void configureViewHolder1(LanguageListViewHolder vh1, int position) {
        LanguageListItem langItem = languageList.get(position);

        if (langItem != null) {
            vh1.getLangTitle().setText(langItem.getTitle());
            String flagName = langItem.getFlagName().toLowerCase();

            //get flagname using it direct name
            Resources resources = mContext.getResources();
            final int resourceId = resources.getIdentifier(flagName, "drawable",
                    mContext.getPackageName());
            resources.getDrawable(resourceId);

            vh1.getCountryFlag().setImageDrawable(resources.getDrawable(resourceId));
        }
    }


}