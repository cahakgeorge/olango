package com.laclicservices.olango.viewbinder.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.laclicservices.olango.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class ChatListViewHolder extends RecyclerView.ViewHolder {

   /** All post views will share this same view holder,
    some non-constant views will be set to nullable to
    allow for sharing of the same holder*/

   //VIEWS shared by all case review card types
    @BindView(R.id._profile_pic) CircleImageView profilePic;
    @BindView(R.id._username)
    TextView username;
    @BindView(R.id._active_icon)
    ImageView icon;

    @BindView(R.id._message_date)
    TextView messageDate;
    @BindView(R.id._message_text)
    TextView messageText;

    @BindView(R.id._pending_text)
    TextView pendingText;

    @BindView(R.id._pending_layout)
    RelativeLayout pendingLayout;

    private Unbinder binder;

    public ChatListViewHolder(View view) {
        super(view);

        binder = ButterKnife.bind(this, view);
    }

    public CircleImageView getProfilePic() {
        return profilePic;
    }

    public TextView getUsername() {
        return username;
    }

    public ImageView getIcon() {
        return icon;
    }

    public TextView getChatDate() {
        return messageDate;
    }

    public TextView getChatText() {
        return messageText;
    }

    public TextView getPendingText(){
        return  pendingText;
    }

    public RelativeLayout getPendingLayout(){
        return pendingLayout;
    }
}
