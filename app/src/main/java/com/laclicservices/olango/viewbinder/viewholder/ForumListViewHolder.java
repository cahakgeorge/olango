package com.laclicservices.olango.viewbinder.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.laclicservices.olango.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class ForumListViewHolder extends RecyclerView.ViewHolder {

   /** All post views will share this same view holder,
    some non-constant views will be set to nullable to
    allow for sharing of the same holder*/

   //VIEWS shared by all case review card types
    @BindView(R.id._forum_pic) CircleImageView forumPic;
    @BindView(R.id._forum_name) TextView forumName;
    @BindView(R.id._forum_text) TextView forumText;

    @BindView(R.id._members_text) TextView membersText;


    private Unbinder binder;

    public ForumListViewHolder(View view) {
        super(view);

        binder = ButterKnife.bind(this, view);
    }

    public CircleImageView getForumPic() {
        return forumPic;
    }

    public TextView getForumName() {
        return forumName;
    }

    public TextView getForumText() {
        return forumText;
    }

    public TextView getMembersText() {
        return membersText;
    }
}
