package com.laclicservices.olango.viewbinder.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.laclicservices.olango.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class ForumMemberListViewHolder extends RecyclerView.ViewHolder {

   /** All post views will share this same view holder,
    some non-constant views will be set to nullable to
    allow for sharing of the same holder*/

   //VIEWS shared by all case review card types
    @BindView(R.id._profile_pic) CircleImageView profilePic;
    @BindView(R.id._username)
    TextView forumName;

    @BindView(R.id._message_date)
    TextView seenDate;
    @BindView(R.id._message_text)
    TextView messageText;

    private Unbinder binder;

    public ForumMemberListViewHolder(View view) {
        super(view);

        binder = ButterKnife.bind(this, view);
    }

    public CircleImageView getProfilePic() {
        return profilePic;
    }

    public TextView getForumName() {
        return forumName;
    }

    public TextView getSeenDate() {
        return seenDate;
    }

    public TextView getText() {
        return messageText;
    }
}
