package com.laclicservices.olango.viewbinder.model;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class ForumList {

    private int messageId;
    private String forumName;
    private String forumText;

    private String forumMembers;

    public ForumList(int messageId, String forumName, String forumText, String forumMembers) {
        this.messageId = messageId;
        this.forumName = forumName;
        this.forumText = forumText;

        this.forumMembers = forumMembers;
    }

    public int getMessageId(){
        return messageId;
    }
    public String getForumName(){
        return forumName;
    }
    public String getForumText(){
        return forumText;
    }

    public String getForumMembers(){
        return forumMembers;
    }
}
