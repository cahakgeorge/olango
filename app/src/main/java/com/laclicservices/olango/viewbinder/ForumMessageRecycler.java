package com.laclicservices.olango.viewbinder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laclicservices.olango.R;
import com.laclicservices.olango.viewbinder.model.ForumMessage;
import com.laclicservices.olango.viewbinder.viewholder.ForumMessageViewHolder;

import java.util.ArrayList;


//gotten from http://www.appifiedtech.net/2015/02/15/android-recyclerview-example/
public class ForumMessageRecycler extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<ForumMessage> forumMessages;

    private final int POST_TYPE_SELF = 0,  POST_TYPE_OTHER= 1;

    public ForumMessageRecycler(Context context, ArrayList<ForumMessage> forumMessages) {
        this.mContext = context;
        this.forumMessages = forumMessages;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.forumMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (forumMessages.get(position).getOwner().equalsIgnoreCase("self")) {
            return POST_TYPE_SELF;
        } else if (forumMessages.get(position).getOwner().equalsIgnoreCase("other")) {
            return POST_TYPE_OTHER;
        }
        return -1;
    }
    /**
     * This method creates different RecyclerView.ViewHolder objects based on the item view type.\
     *
     * @param viewGroup ViewGroup container for the item
     * @param viewType type of view to be inflated
     * @return viewHolder to be inflated
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v = null;
        switch (viewType) {
            case POST_TYPE_SELF:
                v = inflater.inflate(R.layout.message_self_card, viewGroup, false);
                break;
            case POST_TYPE_OTHER:
                v = inflater.inflate(R.layout.message_other_card, viewGroup, false);
                break;
        }

        viewHolder = new ForumMessageViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ForumMessageViewHolder vh = (ForumMessageViewHolder) viewHolder;
        configureViewHolder1(vh, position);
    }

    private void configureViewHolder1(ForumMessageViewHolder vh, int position) {
        ForumMessage message = forumMessages.get(position);
        if (message != null) {
            vh.getMessageText().setText(message.getMessageContent());

            if(vh.getItemViewType() == POST_TYPE_OTHER){ //if message card is for message sender, set a pic resource
                if(position == 0) vh.getProfilePic().setImageResource(R.drawable.avatar1);
                if(position == 1) vh.getProfilePic().setImageResource(R.drawable.avatar2);
                if(position == 2) vh.getProfilePic().setImageResource(R.drawable.avatar3);
                if(position == 3) vh.getProfilePic().setImageResource(R.drawable.cavatar1);
            }

        }
    }

}