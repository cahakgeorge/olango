package com.laclicservices.olango.viewbinder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laclicservices.olango.R;
import com.laclicservices.olango.viewbinder.model.ChatList;
import com.laclicservices.olango.viewbinder.model.ForumMemberList;
import com.laclicservices.olango.viewbinder.viewholder.ChatListViewHolder;
import com.laclicservices.olango.viewbinder.viewholder.ForumListViewHolder;
import com.laclicservices.olango.viewbinder.viewholder.ForumMemberListViewHolder;

import java.util.ArrayList;


//gotten from http://www.appifiedtech.net/2015/02/15/android-recyclerview-example/
public class ForumMemberListRecycler extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<ForumMemberList> memberList;

    public ForumMemberListRecycler(Context context, ArrayList<ForumMemberList> memberList) {
        this.mContext = context;
        this.memberList = memberList;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.memberList.size();
    }

    /**
     * This method creates different RecyclerView.ViewHolder objects based on the item view type.\
     *
     * @param viewGroup ViewGroup container for the item
     * @param viewType type of view to be inflated
     * @return viewHolder to be inflated
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v1 = inflater.inflate(R.layout.forum_members_list_card, viewGroup, false);
        viewHolder = new ForumMemberListViewHolder(v1);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ForumMemberListViewHolder vh1 = (ForumMemberListViewHolder) viewHolder;
        configureViewHolder1(vh1, position);
    }

    private void configureViewHolder1(ForumMemberListViewHolder vh1, int position) {
        ForumMemberList member = memberList.get(position);
        if (member != null) {
            vh1.getForumName().setText(member.getUsername());
            vh1.getText().setText(member.getMessageContent());

            vh1.getSeenDate().setText(member.getDate());

            if(position == 0){
                vh1.getProfilePic().setImageResource(R.drawable.cavatar1);
            }else if(position == 1){
                vh1.getProfilePic().setImageResource(R.drawable.avatar2);
            }else if(position == 2){
                vh1.getProfilePic().setImageResource(R.drawable.cavatar1);
            }else if(position == 3){
                vh1.getProfilePic().setImageResource(R.drawable.avatar4);
            }else if(position == 4){
                vh1.getProfilePic().setImageResource(R.drawable.cavatar3);
            }else vh1.getProfilePic().setImageResource(R.drawable.cavatar1);

        }
    }


}