package com.laclicservices.olango.viewbinder.model;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class Chats {

    private int messageId;
    private String owner;

    private String date;
    private String messageContent;

    public Chats(int messageId, String owner, String date, String messageContent) {
        this.messageId = messageId;
        this.owner = owner;

        this.date = date;
        this.messageContent = messageContent;
    }

    public int getMessageId(){
        return messageId;
    }
    public String getOwner(){
        return owner;
    }

    public String getDate(){
        return date;
    }
    public String getMessageContent(){
        return messageContent;
    }
}
