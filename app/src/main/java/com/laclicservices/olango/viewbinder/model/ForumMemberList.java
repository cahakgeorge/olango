package com.laclicservices.olango.viewbinder.model;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class ForumMemberList {

    private int messageId;
    private String username;
    private String date;

    private String messageContent;

    public ForumMemberList(int messageId, String username, String date, String messageContent) {
        this.messageId = messageId;
        this.username = username;

        this.date = date;
        this.messageContent = messageContent;
    }

    public int getMessageId(){
        return messageId;
    }
    public String getUsername(){
        return username;
    }

    public String getDate(){
        return date;
    }
    public String getMessageContent(){
        return messageContent;
    }
}
