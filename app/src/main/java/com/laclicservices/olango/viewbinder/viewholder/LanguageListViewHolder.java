package com.laclicservices.olango.viewbinder.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.laclicservices.olango.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class LanguageListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id._country_flag)
    CircleImageView countryFlag;

    @BindView(R.id._languageTitle)
    TextView langTitle;


    private Unbinder binder;

    public LanguageListViewHolder(View view, Context mContext) {
        super(view);

        binder = ButterKnife.bind(this, view);
    }

    public CircleImageView getCountryFlag() {
        return countryFlag;
    }

    public TextView getLangTitle() {
        return langTitle;
    }

}
