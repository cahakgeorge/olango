package com.laclicservices.olango.viewbinder.viewholder;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.laclicservices.olango.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class ChatViewHolder extends RecyclerView.ViewHolder {

   /** All post views will share this same view holder,
    some non-constant views will be set to nullable to
    allow for sharing of the same holder*/

   //VIEWS shared by all case review card types
    @Nullable @BindView(R.id._profile_pic) CircleImageView profilePic;
    @BindView(R.id._message_text)
    TextView messageText;

    //@BindView(R.id._message_date) TextView messageDate;


    private Unbinder binder;

    public ChatViewHolder(View view) {
        super(view);

        binder = ButterKnife.bind(this, view);
    }

    @Nullable
    public CircleImageView getProfilePic() {
        return profilePic;
    }

    public TextView getMessageText() {
        return messageText;
    }

}
