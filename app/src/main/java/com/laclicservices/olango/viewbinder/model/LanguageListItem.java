package com.laclicservices.olango.viewbinder.model;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class LanguageListItem {

    private String title;
    private String flagName;

    public LanguageListItem(String title, String flagName) {
        this.title = title;
        this.flagName = flagName;
    }

    public String getTitle(){
        return title;
    }
    public String getFlagName(){
        return flagName;
    }

}
