package com.laclicservices.olango.viewbinder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laclicservices.olango.R;
import com.laclicservices.olango.viewbinder.model.ChatList;
import com.laclicservices.olango.viewbinder.viewholder.ChatListViewHolder;

import java.util.ArrayList;


//gotten from http://www.appifiedtech.net/2015/02/15/android-recyclerview-example/
public class ChatListRecycler extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<ChatList> chatList;

    public ChatListRecycler(Context context, ArrayList<ChatList> chatList) {
        this.mContext = context;
        this.chatList = chatList;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.chatList.size();
    }

    /**
     * This method creates different RecyclerView.ViewHolder objects based on the item view type.\
     *
     * @param viewGroup ViewGroup container for the item
     * @param viewType type of view to be inflated
     * @return viewHolder to be inflated
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v1 = inflater.inflate(R.layout.chat_list_card, viewGroup, false);
        viewHolder = new ChatListViewHolder(v1);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ChatListViewHolder vh1 = (ChatListViewHolder) viewHolder;
        configureViewHolder1(vh1, position);
    }

    private void configureViewHolder1(ChatListViewHolder vh1, int position) {
        ChatList message = chatList.get(position);
        if (message != null) {
            vh1.getUsername().setText(message.getUsername());
            vh1.getChatDate().setText(message.getDate());

            vh1.getChatText().setText(message.getMessageContent());

            if(message.getUserStatus().equalsIgnoreCase("active")){
                vh1.getIcon().setColorFilter(ContextCompat.getColor(mContext, R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
            }else vh1.getIcon().setColorFilter(ContextCompat.getColor(mContext, R.color.Red), android.graphics.PorterDuff.Mode.SRC_IN);

            if(position == 0){
                vh1.getProfilePic().setImageResource(R.drawable.avatar3);
            }else if(position == 1){
                vh1.getProfilePic().setImageResource(R.drawable.avatar2);
            }else if(position == 2){
                vh1.getProfilePic().setImageResource(R.drawable.cavatar1);
            }else if(position == 3){
                vh1.getProfilePic().setImageResource(R.drawable.avatar4);
            }else if(position == 4){
                vh1.getProfilePic().setImageResource(R.drawable.cavatar3);
            }

            if(!"".equalsIgnoreCase(message.getPendingMsg())){ //if pending msg is not null
                vh1.getPendingText().setText(message.getPendingMsg());
            }else vh1.getPendingLayout().setVisibility(View.GONE);

        }
    }


}