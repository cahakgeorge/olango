package com.laclicservices.olango.viewbinder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laclicservices.olango.R;
import com.laclicservices.olango.viewbinder.model.ForumList;
import com.laclicservices.olango.viewbinder.viewholder.ForumListViewHolder;

import java.util.ArrayList;


//gotten from http://www.appifiedtech.net/2015/02/15/android-recyclerview-example/
public class ForumListRecycler extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<ForumList> forumList;

    public ForumListRecycler(Context context, ArrayList<ForumList> forumList) {
        this.mContext = context;
        this.forumList = forumList;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.forumList.size();
    }

    /**
     * This method creates different RecyclerView.ViewHolder objects based on the item view type.\
     *
     * @param viewGroup ViewGroup container for the item
     * @param viewType type of view to be inflated
     * @return viewHolder to be inflated
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v1 = inflater.inflate(R.layout.forum_list_card, viewGroup, false);
        viewHolder = new ForumListViewHolder(v1);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ForumListViewHolder vh1 = (ForumListViewHolder) viewHolder;
        configureViewHolder1(vh1, position);
    }

    private void configureViewHolder1(ForumListViewHolder vh1, int position) {
        ForumList forum = forumList.get(position);
        if (forum != null) {
            vh1.getForumName().setText(forum.getForumName());
            vh1.getForumText().setText(forum.getForumText());

            vh1.getMembersText().setText(forum.getForumMembers());

            if(position == 0){
                vh1.getForumPic().setImageResource(R.drawable.english);
            }else if(position == 1){
                vh1.getForumPic().setImageResource(R.drawable.tab_forum);
            }else if(position == 2){
                vh1.getForumPic().setImageResource(R.drawable.portugal);
            }else if(position == 3){
                vh1.getForumPic().setImageResource(R.drawable.france);
            }else if(position == 4){
                vh1.getForumPic().setImageResource(R.drawable.germany);
            }

        }
    }


}