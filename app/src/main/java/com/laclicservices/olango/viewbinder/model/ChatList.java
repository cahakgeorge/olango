package com.laclicservices.olango.viewbinder.model;

/**
 * Created by Cahaksw on 26/12/2017.
 */

public class ChatList {

    private int messageId;
    private String username;
    private String userStatus;
    private String date;

    private String pendingMsg;
    private String messageContent;

    public ChatList(int messageId, String username, String userStatus, String date, String messageContent, String pendingMsg) {
        this.messageId = messageId;
        this.username = username;
        this.userStatus = userStatus;

        this.date = date;
        this.pendingMsg = pendingMsg;
        this.messageContent = messageContent;
    }

    public int getMessageId(){
        return messageId;
    }
    public String getUsername(){
        return username;
    }
    public String getUserStatus(){
        return userStatus;
    }

    public String getDate(){
        return date;
    }
    public String getMessageContent(){
        return messageContent;
    }

    public String getPendingMsg(){
        return pendingMsg;
    }
}
