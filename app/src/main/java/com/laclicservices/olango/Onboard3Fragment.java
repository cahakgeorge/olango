package com.laclicservices.olango;


import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.laclicservices.olango.config.Conf;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class Onboard3Fragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    public Onboard3Fragment() {
        // Required empty public constructor
    }


    @BindView(R.id._title)
    TextView title;
    @BindView(R.id._text)
    TextView text;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_onboard3, container, false);
        ButterKnife.bind(this, mRootView);

        Conf conf = new Conf(getActivity());
        title.setTypeface(conf.getFont("medium"));
        text.setTypeface(conf.getFont("medium"));

        return mRootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
