package com.laclicservices.olango;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.laclicservices.olango.config.Conf;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    @BindView(R.id._forgot_title) TextView title;
    @BindView(R.id._prompt_text) TextView forgotPrompt;

    @BindView(R.id._reset_password) Button resetPassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        binder = ButterKnife.bind(this, mRootView);

        Conf conf = new Conf(getActivity());
        title.setTypeface(conf.getFont("heavy"));
        forgotPrompt.setTypeface(conf.getFont("heavy"));
        resetPassword.setTypeface(conf.getFont("medium"));

        return mRootView;
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
