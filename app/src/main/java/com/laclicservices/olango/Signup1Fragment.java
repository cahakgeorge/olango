package com.laclicservices.olango;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.laclicservices.olango.config.Conf;
import com.laclicservices.olango.viewbinder.LanguageListRecycler;
import com.laclicservices.olango.viewbinder.helper.ItemClickSupport;
import com.laclicservices.olango.viewbinder.model.LanguageListItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class Signup1Fragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    public Signup1Fragment() {
        // Required empty public constructor
    }

    @BindView(R.id._prompt) TextView toolbarPrompt;
    @BindView(R.id._recycler_view_countries) RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_signup1, container, false);
        binder = ButterKnife.bind(this, mRootView);

        Conf conf = new Conf(getActivity());
        toolbarPrompt.setTypeface(conf.getFont("heavy"));

        this.setupRecyclerViewLanguage(); //Recycler view to fill in the corresponding content

        return mRootView;
    }

    @OnClick(R.id._back) void backPressed(){
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    public void setupRecyclerViewLanguage() {
        //get Post data
        ArrayList<LanguageListItem> langItems = this.getTransactions();

        // Create adapter passing in the sample user data
        LanguageListRecycler adapter = new LanguageListRecycler(getActivity(), langItems);

        // Attach the adapter to the recyclerview to populate items
        recyclerView.setAdapter(adapter);

        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        this.setupItemClickHandler();
        // That's all!
    }

    protected static ArrayList<LanguageListItem> getTransactions(){

        String[] countryName = new String[]{"English","French","German","Italian","Portuguese","Dutch","Irish","Swedish","Turkish","Spanish"};
        String[] countryFlag = new String[]{"english","france","germany","italy","portugal","dutch","ireland","sweden","turkey","spain"};

        ArrayList<LanguageListItem> langList = new ArrayList<>();

        for (int i = 0; i < countryName.length; i++) {
            langList.add(new LanguageListItem(countryName[i], countryFlag[i]) );
        }

        return langList;
    }

    private void setupItemClickHandler(){
        final Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Toast.makeText(getActivity(), "Item clicked", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
