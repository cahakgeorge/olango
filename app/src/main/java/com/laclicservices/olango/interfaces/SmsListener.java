package com.laclicservices.olango.interfaces;

/**
 * Created by Cahaksw on 21/11/2017.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}
