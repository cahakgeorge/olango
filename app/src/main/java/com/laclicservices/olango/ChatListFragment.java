package com.laclicservices.olango;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.laclicservices.olango.viewbinder.ChatListRecycler;
import com.laclicservices.olango.viewbinder.helper.ItemClickSupport;
import com.laclicservices.olango.viewbinder.model.ChatList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatListFragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    @BindView(R.id._title) TextView chats;
    //@BindView(R.id._chat) ImageView chatIcon;
    @BindView(R.id._more) ImageView more;

    public ChatListFragment() {
        // Required empty public constructor
    }

    @BindView(R.id._recycler_view_message_list) RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_chat_list, container, false);
        binder = ButterKnife.bind(this, mRootView);

        //setup Recyclerview to handle posts population
        this.setupRecyclerView();

        return mRootView;
    }


    protected void setupRecyclerView(){
        //get Post data
        ArrayList<ChatList> chatList = this.getMessageData();

        // Create adapter passing in the sample user data
        ChatListRecycler adapter = new ChatListRecycler(getActivity(), chatList);
        // Attach the adapter to the recyclerview to populate items
        recyclerView.setAdapter(adapter);

        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        this.setupItemClickHandler();
        // That's all!
    }

    protected ArrayList<ChatList> getMessageData(){
        //ArrayList postInfo, ArrayList postActions, ArrayList postContent
        String[] usernames = new String[]{getResources().getString(R.string.chat_name1), getResources().getString(R.string.chat_name2), getResources().getString(R.string.chat_name3),getResources().getString(R.string.chat_name4),getResources().getString(R.string.chat_name5), getResources().getString(R.string.chat_name1)};
        String[] userStatus = new String[]{"active","active","inactive","active","inactive","inactive"};
        String[] dates = new String[]{getResources().getString(R.string.chat_date1), getResources().getString(R.string.chat_date2), getResources().getString(R.string.chat_date3),getResources().getString(R.string.chat_date4), getResources().getString(R.string.chat_date5), getResources().getString(R.string.chat_date1)};

        ArrayList<ChatList> chatList = new ArrayList<>();

        for (int i = 0; i < usernames.length; i++) {

            String message = getResources().getString(R.string.chat1);
            String pendingMsg = "";
            if(i==0 || i==1){
                pendingMsg = "3";
            }
            chatList.add(new ChatList(i, usernames[i], userStatus[i], dates[i], message, pendingMsg));
        }

        return chatList;
    }

    private void setupItemClickHandler(){
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        // do it
                        //Toast.makeText(getActivity(), "Item Clicked", Toast.LENGTH_LONG).show();
                        Fragment frag = new ChatUserFragment();

                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.popup_enter, 0);
                        fragmentTransaction.add(R.id._parent_frame, frag, "chat_user_fragment");
                        //fragmentTransaction.addToBackStack(null);
                        //fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        fragmentTransaction.commit();
                    }
                }
        );
    }


    @OnClick(R.id._back) void backImageClicked(){
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @OnClick(R.id._more) void moreClicked(){
        View menuItemView = more;
        PopupMenu popupMenu = new PopupMenu(getActivity(), menuItemView);
        popupMenu.getMenuInflater().inflate(R.menu.chat_more_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_sort:
                        Toast.makeText(getActivity(), "Sort action failed", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_mute:
                        Toast.makeText(getActivity(), "Muted", Toast.LENGTH_SHORT).show();
                    case R.id.action_media:
                        Toast.makeText(getActivity(), "No media for now", Toast.LENGTH_SHORT).show();
                    default:
                        break;

                }
                return true;
            }
        });
        popupMenu.show();
    }

    /*@OnClick(R.id._chat) void chatIconClicked(){
        View menuItemView = chatIcon;
        PopupMenu popupMenu = new PopupMenu(getActivity(), menuItemView);
        popupMenu.getMenuInflater().inflate(R.menu.chat_more_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_sort:
                        Toast.makeText(getActivity(), "Sort action failed", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_mute:
                        Toast.makeText(getActivity(), "Muted", Toast.LENGTH_SHORT).show();
                    case R.id.action_media:
                        Toast.makeText(getActivity(), "No media for now", Toast.LENGTH_SHORT).show();
                    default:
                        break;

                }
                return true;
            }
        });
        popupMenu.show();
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
