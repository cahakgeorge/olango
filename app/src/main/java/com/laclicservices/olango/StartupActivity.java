package com.laclicservices.olango;

import android.content.res.Resources;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class StartupActivity extends AppCompatActivity {

    private Unbinder binder;

    private CarouselFragment carouselFragment;
    private GetStartedFragment getStarted;

    //get flagname using it direct name
    /*Resources resources = mContext.getResources();
    final int resourceId = resources.getIdentifier(flagName, "drawable",
            mContext.getPackageName());
            resources.getDrawable(resourceId);
            */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        binder = ButterKnife.bind(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        //TODO: adda check to see if user has gone through onboarding process
        if (savedInstanceState == null) {
            // withholding the previously created fragment from being created again
            // On orientation change, it will prevent fragment recreation
            // its necessary to reserve the fragment stack inside each tab
            //getStarted();
            initScreen();
        } else {
            // restoring the previously created fragment
            // and getting the reference
            carouselFragment = (CarouselFragment) getSupportFragmentManager().getFragments().get(0);
        }
    }

    private void initScreen() {
        // Creating the ViewPager container fragment once
        carouselFragment = new CarouselFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.popup_enter, 0);
        fragmentTransaction.add(R.id._startup_frame, carouselFragment, "onboarding_fragment");
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        Fragment carouselFragment = getSupportFragmentManager().findFragmentByTag("onboarding_fragment");
        Fragment getStartedFragment = getSupportFragmentManager().findFragmentByTag("get_started_fragment");
        Fragment loginFragment = getSupportFragmentManager().findFragmentByTag("login_fragment");
        Fragment forgotPasswordFragment = getSupportFragmentManager().findFragmentByTag("forgot_password_fragment");
        Fragment signup1Fragment = getSupportFragmentManager().findFragmentByTag("signup1_fragment");

        if (carouselFragment != null) {//exit app
            finish();
        }else if (forgotPasswordFragment != null) {//remove forgot password fragment
            removeFragment(forgotPasswordFragment);
        }else if (loginFragment != null) {//remove login fragment
            removeFragment(loginFragment);
        }else if (signup1Fragment != null) {//remove login fragment
            removeFragment(signup1Fragment);
        }else if (getStartedFragment != null) {//exit app
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void removeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }
}
