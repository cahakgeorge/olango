package com.laclicservices.olango;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laclicservices.olango.config.Conf;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class GetStartedFragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    public GetStartedFragment() {
        // Required empty public constructor
    }

    @BindView(R.id._hi_title) TextView hiThere;
    @BindView(R.id._my_name_text) TextView myNameIs;

    @BindView(R.id._cant_wait_text) TextView cantWait;
    @BindView(R.id._get_started) Button getStarted;

    @BindView(R.id._already_started) TextView alStarted;
    @BindView(R.id._login) TextView login;

    @BindView(R.id._login_layout) LinearLayout loginLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_get_started, container, false);
        binder = ButterKnife.bind(this, mRootView);

        Conf conf = new Conf(getActivity());
        hiThere.setTypeface(conf.getFont("black"));
        myNameIs.setTypeface(conf.getFont("medium"));
        cantWait.setTypeface(conf.getFont("medium"));
        getStarted.setTypeface(conf.getFont("medium"));
        alStarted.setTypeface(conf.getFont("heavy"));
        login.setTypeface(conf.getFont("black"));
        this.setupNameSpans();

        return mRootView;
    }

    public void setupNameSpans(){
        Spannable WordtoSpan = new SpannableString(getActivity().getString(R.string.my_name_is));

        WordtoSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorPrimary)), 0, 11, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        WordtoSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorBlack4F)), 11, 17, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        WordtoSpan.setSpan(new TextAppearanceSpan(getActivity(), Typeface.ITALIC), 11, 17, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        myNameIs.setText(WordtoSpan);
    }

    @OnClick(R.id._login_layout) void loginClicked(){
        Fragment frag = new LoginFragment();

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.sliderightenter, 0);
        fragmentTransaction.add(R.id._startup_frame, frag, "login_fragment");
        fragmentTransaction.commit();
    }

    @OnClick(R.id._get_started) void showLangClicked(){
        Fragment frag = new Signup1Fragment();

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.sliderightenter, 0);
        fragmentTransaction.add(R.id._startup_frame, frag, "signup1_fragment");
        fragmentTransaction.commit();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
