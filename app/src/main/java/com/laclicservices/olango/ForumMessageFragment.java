package com.laclicservices.olango;


import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.laclicservices.olango.viewbinder.ForumMessageRecycler;
import com.laclicservices.olango.viewbinder.model.ForumMessage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForumMessageFragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    @BindView(R.id.app_bar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id._recycler_view_message) RecyclerView recyclerView;
    @BindView(R.id._image) ImageView image;
    @BindView(R.id._more) ImageView more;

    public ForumMessageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_forum_message, container, false);
        binder = ButterKnife.bind(this, mRootView);

        //ViewCompat.setTransitionName(appBarLayout, R.drawable.arrow_back_green_24dp);

        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(getActivity(),android.R.color.white));

        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.english_background)).getBitmap();
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette palette) {
                applyPalette(palette);
            }
        });

        //setup Recyclerview to handle posts population
        this.setupRecyclerView();

        return mRootView;
    }

    private void applyPalette(Palette palette) {
        int primaryDark = ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark);
        int primary = ContextCompat.getColor(getActivity(),R.color.colorPrimary);
        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
    }

    protected void setupRecyclerView(){
        //get Post data
        ArrayList<ForumMessage> messages = this.getMessageData();

        // Create adapter passing in the sample user data
        ForumMessageRecycler adapter = new ForumMessageRecycler(getActivity(), messages);
        // Attach the adapter to the recyclerview to populate items
        recyclerView.setAdapter(adapter);

        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        this.setupItemClickHandler();
        // That's all!
    }

    protected ArrayList<ForumMessage> getMessageData(){
        //ArrayList postInfo, ArrayList postActions, ArrayList postContent
        String[] messages = new String[]{getResources().getString(R.string.fragment_message_conv_o1), getResources().getString(R.string.fragment_message_conv_o2),getResources().getString(R.string.fragment_message_conv_o3),getResources().getString(R.string.fragment_message_conv_me2), getResources().getString(R.string.fragment_message_conv_me2)};
        String[] owner = new String[]{"other","other","other","self","other"};
        String[] dates = new String[]{getResources().getString(R.string.fragment_message_date1), getResources().getString(R.string.fragment_message_date2), getResources().getString(R.string.fragment_message_date3),getResources().getString(R.string.fragment_message_date3), getResources().getString(R.string.fragment_message_date3)};

        ArrayList<ForumMessage> messageList = new ArrayList<>();

        for (int i = 0; i < messages.length; i++) {
            messageList.add(new ForumMessage(i, owner[i], dates[i], messages[i]));
        }

        return messageList;
    }

    private void setupItemClickHandler(){
    }

    @OnClick(R.id._back) void backImageClicked(){
        //getFragmentManager().popBackStack();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @OnClick(R.id._more) void moreClicked(){
        View menuItemView = more;
        PopupMenu popupMenu = new PopupMenu(getActivity(), menuItemView);
        popupMenu.getMenuInflater().inflate(R.menu.message_more_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_view_member:
                        openForumMembersFragment();
                        break;
                    case R.id.action_media:
                        Toast.makeText(getActivity(), "No media yet", Toast.LENGTH_LONG).show();
                    case R.id.action_exit_forum:
                        Toast.makeText(getActivity(), "Can't exit forum now", Toast.LENGTH_LONG).show();
                    case R.id.action_mute:
                        Toast.makeText(getActivity(), "Mute clicked", Toast.LENGTH_LONG).show();
                    default:
                        break;

                }
                return true;
            }
        });
        popupMenu.show();
    }

    public void openForumMembersFragment(){
        Fragment frag = new ForumMembersListFragment();

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.popup_enter, 0);
        fragmentTransaction.add(R.id._parent_frame, frag, "forum_members_fragment");
        fragmentTransaction.commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
