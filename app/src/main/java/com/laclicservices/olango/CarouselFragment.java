package com.laclicservices.olango;


import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.UnderlineSpan;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.ToxicBakery.viewpager.transforms.DefaultTransformer;
import com.laclicservices.olango.config.Conf;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class CarouselFragment extends Fragment {

    //private static final String TAG = "SignInFragment";
    View mRootView;
    private Unbinder binder;
    private static final String TAG = CarouselFragment.class.getSimpleName();


    //Now add bindings using butter-knife
    @BindView(R.id.view_pager) public ViewPager mViewPager;
    @BindView(R.id.circle_indicator) public CirclePageIndicator titleIndicator;

    @BindView(R.id._skip_continue) public TextView skipContinue;

    private MyViewPagerAdapter mViewPagerAdapter;


    public CarouselFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_carousel, container, false);
        binder = ButterKnife.bind(this, mRootView);

        Conf conf = new Conf(getActivity());
        skipContinue.setTypeface(conf.getFont("heavy"));

        // making notification bar transparent
        changeStatusBarColor();

        this.setupPrivacyText();

        titleIndicator.setFillColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

        return mRootView;
    }

    public void setupPrivacyText(){
        Spannable WordtoSpan = new SpannableString(getActivity().getString(R.string.skip));
        WordtoSpan.setSpan(new UnderlineSpan(), 0, WordtoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        skipContinue.setText(WordtoSpan);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Note that we are passing childFragmentManager, not FragmentManager
        mViewPagerAdapter = new MyViewPagerAdapter(getResources(), getChildFragmentManager());

        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        mViewPager.setPageTransformer(false, new DefaultTransformer()); // Set a PageTransformer

        titleIndicator.setViewPager(mViewPager);
    }

    @OnClick(R.id._skip_continue) void skipClicked(){

        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();

        Fragment frag = new GetStartedFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.popup_enter, 0);
        fragmentTransaction.add(R.id._startup_frame, frag, "get_started_fragment");
        fragmentTransaction.commit();

    }

    protected void makeNotificationBarInVisible(Boolean invisible) {
        if (Build.VERSION.SDK_INT >= 21) {
            if (invisible)
                getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            else
                getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_VISIBLE);

        }
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageSelected(int position) {
            if(position == 1) titleIndicator.setFillColor(ContextCompat.getColor(getActivity(), R.color.white));
            else titleIndicator.setFillColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

            if(position == 1) skipContinue.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            else skipContinue.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

            if(position == 2) skipContinue.setText(getResources().getString(R.string.continue_));
            else skipContinue.setText(getResources().getString(R.string.skip));
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };


    public class MyViewPagerAdapter extends FragmentPagerAdapter {

        private final Resources resources;

        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        /**
         * Create pager adapter
         * @param resources
         * @param fm
         */
        public MyViewPagerAdapter(final Resources resources, FragmentManager fm) {
            super(fm);
            this.resources = resources;
        }

        @Override
        public Fragment getItem(int position) {
            final Fragment result;
            switch (position) {
                case 0:
                    // First Fragment of First Tab
                    result = new Onboard1Fragment();
                    break;
                case 1:
                    // First Fragment of Second Tab
                    result = new Onboard2Fragment();
                    break;
                case 2:
                    // First Fragment of Third Tab
                    result = new Onboard3Fragment();
                    break;
                default:
                    result = null;
                    break;
            }

            return result;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(final int position) {
           return null;
        }

        /**
         * On each Fragment instantiation we are saving the reference of that Fragment in a Map
         * It will help us to retrieve the Fragment by position
         * @param container
         * @param position
         * @return
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        /**
         * Remove the saved reference from our Map on the Fragment destroy
         * @param container
         * @param position
         * @param object
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        /**
         * Get the Fragment by position
         * @param position tab position of the fragment
         * @return
         */
        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }

}