package com.laclicservices.olango;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.laclicservices.olango.viewbinder.ChatRecycler;
import com.laclicservices.olango.viewbinder.model.Chats;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatUserFragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    @BindView(R.id._title) TextView chats;
    @BindView(R.id._more) ImageView more;

    public ChatUserFragment() {
        // Required empty public constructor
    }

    @BindView(R.id._recycler_view_messages) RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_chat_user, container, false);
        binder = ButterKnife.bind(this, mRootView);

        //setup Recyclerview to handle posts population
        this.setupRecyclerView();

        return mRootView;
    }

    protected void setupRecyclerView(){
        //get Post data
        ArrayList<Chats> chatMessages = this.getChatData();

        // Create adapter passing in the sample user data
        ChatRecycler adapter = new ChatRecycler(getActivity(), chatMessages);
        // Attach the adapter to the recyclerview to populate items
        recyclerView.setAdapter(adapter);

        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // That's all!
    }

    protected ArrayList<Chats> getChatData(){
        //ArrayList postInfo, ArrayList postActions, ArrayList postContent
        String[] messages = new String[]{getResources().getString(R.string.fragment_message_conv_o1), getResources().getString(R.string.fragment_message_conv_me1), getResources().getString(R.string.fragment_message_conv_o2),getResources().getString(R.string.fragment_message_conv_me2),getResources().getString(R.string.fragment_message_conv_o3), getResources().getString(R.string.fragment_message_conv_me1),getResources().getString(R.string.fragment_message_conv_o1),getResources().getString(R.string.fragment_message_conv_o1), getResources().getString(R.string.fragment_message_conv_me1), getResources().getString(R.string.fragment_message_conv_o2)};
        String[] owner = new String[]{"other","self","other","self","other","self","other","other","self","other"};
        String[] dates = new String[]{getResources().getString(R.string.fragment_message_date1), getResources().getString(R.string.fragment_message_date2), getResources().getString(R.string.fragment_message_date3),getResources().getString(R.string.fragment_message_date3), getResources().getString(R.string.fragment_message_date3), getResources().getString(R.string.fragment_message_date1), getResources().getString(R.string.fragment_message_date2),getResources().getString(R.string.fragment_message_date1), getResources().getString(R.string.fragment_message_date2), getResources().getString(R.string.fragment_message_date3)};

        ArrayList<Chats> chats = new ArrayList<>();

        for (int i = 0; i < messages.length; i++) {
            chats.add(new Chats(i, owner[i], dates[i], messages[i]));
        }

        return chats;
    }

    @OnClick(R.id._back) void backImageClicked(){
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @OnClick(R.id._more) void moreClicked(){
        View menuItemView = more;
        PopupMenu popupMenu = new PopupMenu(getActivity(), menuItemView);
        popupMenu.getMenuInflater().inflate(R.menu.chat_more_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_sort:
                        Toast.makeText(getActivity(), "Sort action failed", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_mute:
                        Toast.makeText(getActivity(), "Muted", Toast.LENGTH_SHORT).show();
                    case R.id.action_media:
                        Toast.makeText(getActivity(), "No media for now", Toast.LENGTH_SHORT).show();
                    default:
                        break;
                }
                return true;
            }
        });
        popupMenu.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
