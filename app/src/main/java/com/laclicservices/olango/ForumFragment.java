package com.laclicservices.olango;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.laclicservices.olango.config.Conf;
import com.laclicservices.olango.viewbinder.ChatListRecycler;
import com.laclicservices.olango.viewbinder.ForumListRecycler;
import com.laclicservices.olango.viewbinder.helper.ItemClickSupport;
import com.laclicservices.olango.viewbinder.model.ChatList;
import com.laclicservices.olango.viewbinder.model.ForumList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForumFragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    public ForumFragment() {
        // Required empty public constructor
    }

    @BindView(R.id._recycler_view_forum_list) RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_forum_list, container, false);
        binder = ButterKnife.bind(this, mRootView);

        //setup Recyclerview to handle posts population
        this.setupRecyclerView();

        return mRootView;
    }

    protected void setupRecyclerView(){
        //get Post data
        ArrayList<ForumList> forumList = this.getMessageData();

        // Create adapter passing in the sample user data
        ForumListRecycler adapter = new ForumListRecycler(getActivity(), forumList);
        // Attach the adapter to the recyclerview to populate items
        recyclerView.setAdapter(adapter);

        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        this.setupItemClickHandler();
        // That's all!
    }

    protected ArrayList<ForumList> getMessageData(){
        //ArrayList postInfo, ArrayList postActions, ArrayList postContent
        String[] forumNames = new String[]{getResources().getString(R.string.qroom), getResources().getString(R.string.indians),getResources().getString(R.string.ngstudents),getResources().getString(R.string.gmnazi)};
        String[] forumMembers = new String[]{"23","3","5","56"};

        ArrayList<ForumList> forumList = new ArrayList<>();

        for (int i = 0; i < forumNames.length; i++) {
            String message = getResources().getString(R.string.ilit_royal);
            if(i==1){
                message = getResources().getString(R.string.indy_only);
            }
            forumList.add(new ForumList(i, forumNames[i], message, forumMembers[i]));
        }
        return forumList;
    }

    private void setupItemClickHandler(){
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        // do it
                        //Toast.makeText(getActivity(), "Item Clicked", Toast.LENGTH_LONG).show();
                        Fragment frag = new ForumMessageFragment();

                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.popup_enter, 0);
                        fragmentTransaction.add(R.id._parent_frame, frag, "forum_message_fragment");
                        //fragmentTransaction.addToBackStack(null);
                        //fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        fragmentTransaction.commit();
                    }
                }
        );
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
