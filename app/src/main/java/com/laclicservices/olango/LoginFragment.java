package com.laclicservices.olango;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.laclicservices.olango.config.Conf;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    View mRootView;
    private Unbinder binder;

    public LoginFragment() {
        // Required empty public constructor
    }

    @BindView(R.id._olango_text) TextView olangoTitle;
    //@BindView(R.id._instantly_text) TextView instantlyText;
    @BindView(R.id._ready_text) TextView readyText;
    @BindView(R.id._forgot_password) TextView forgotPassword;
    @BindView(R.id._one_tap) TextView oneTap;

    @BindView(R.id._username) EditText username;
    @BindView(R.id._password) EditText password;
    @BindView(R.id._sign_in) Button signIn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_login, container, false);
        binder = ButterKnife.bind(this, mRootView);

        Conf conf = new Conf(getActivity());
        olangoTitle.setTypeface(conf.getFont("rez"));
        //instantlyText.setTypeface(conf.getFont("heavy"));
        readyText.setTypeface(conf.getFont("medium"));
        forgotPassword.setTypeface(conf.getFont("medium"));
        oneTap.setTypeface(conf.getFont("medium"));
        username.setTypeface(conf.getFont("medium"));
        password.setTypeface(conf.getFont("medium"));
        signIn.setTypeface(conf.getFont("medium"));

        return mRootView;
    }

    @OnClick(R.id._forgot_password) void forgotPasswordClicked(){
        Fragment frag = new ForgotPasswordFragment();

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.sliderightenter, 0);
        fragmentTransaction.add(R.id._startup_frame, frag, "forgot_password_fragment");
        fragmentTransaction.commit();
    }

    @OnClick(R.id._sign_in) void loginClicked(){
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            binder.unbind();
        }catch(Exception ex){

        }
    }

    //https://inthecheesefactory.com/blog/fragment-state-saving-best-practices/en
    //http://www.techotopia.com/index.php/Saving_and_Restoring_Activity_State_in_Android_Studio
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
