package com.laclicservices.olango;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener  {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private Unbinder binder;
    private Toast toast;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.nav_view) NavigationView mNavigationView;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.tabs) TabLayout tabLayout;

    String[] pageTitle =  new String[]{"class","hero","stats","forums"};;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        binder = ButterKnife.bind(this);

        toolbar.setTitle("Class");
        //toolbar.setTitleTextAppearance(this, R.style.AppTheme_ToolbarTitle);
        setSupportActionBar(toolbar);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        this.setupViewpagerWithTabs();

        this.setupDrawerLayout(toolbar);

        tabLayout.getTabAt(0).setText(pageTitle[0]);//set Title of first default title
    }

    protected void setupViewpagerWithTabs(){
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        //the system keep 2 page instances on both sides of the current page
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if(position==0) HomeActivity.this.toolbar.setTitle("Class");
                else if(position==1) HomeActivity.this.toolbar.setTitle("Hero");
                else if(position==2) HomeActivity.this.toolbar.setTitle("Stats");
                else if(position==3) HomeActivity.this.toolbar.setTitle("Forums");

                clearOtherText(position, tabLayout);
                tabLayout.getTabAt(position).setText(pageTitle[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setAdapter(mSectionsPagerAdapter);

        //animate the transformer   https://guides.codepath.com/android/ViewPager-with-FragmentPagerAdapter
        //AccordionTransformer, RotateUpTransformer, FlipHorizontalTransformer, ScaleInOutTransformer, ZoomInTransformer
        //mViewPager.setPageTransformer(true, new CubeInTransformer()); //others: https://github.com/ToxicBakery/ViewPagerTransforms/tree/master/library/src/main/java/com/ToxicBakery/viewpager/transforms

        tabLayout.setupWithViewPager(mViewPager);

        //An array containing your icons from the drawable directory
        final int[] ICONS = new int[]{
                R.drawable.tab_presentation,
                R.drawable.tab_user,
                R.drawable.tab_podium,
                R.drawable.tab_group
        };

        //tabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.colorPrimary), ContextCompat.getColor(this, R.color.white));
        //tabLayout.setBackgroundColor(Color.WHITE);
        //set the tab indicator color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //if android phone's version is version 23
            tabLayout.setSelectedTabIndicatorColor(getColor(R.color.white));
        }else{//lower android phones
            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.white));
        }
        tabLayout.setupWithViewPager(mViewPager);

        //attach tab icons now
        tabLayout.getTabAt(0).setIcon(ICONS[0]);
        tabLayout.getTabAt(1).setIcon(ICONS[1]);
        tabLayout.getTabAt(2).setIcon(ICONS[2]);
        tabLayout.getTabAt(3).setIcon(ICONS[3]);
    }

    private void clearOtherText(int position, TabLayout tabLayout){
        tabLayout.getTabAt(0).setText("");
        tabLayout.getTabAt(1).setText("");
        tabLayout.getTabAt(2).setText("");
        tabLayout.getTabAt(3).setText("");
    }

    protected void setupDrawerLayout(Toolbar toolbar) {
        //mNavigationView.setItemIconTintList(null);//remove all tints from nav drawer icons

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };

        drawer.addDrawerListener(toggle);
        getSupportActionBar().setHomeButtonEnabled(true);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setItemIconTintList(null);
    }

    @Override
    public void onBackPressed() {

        Fragment chatUserFragment = getSupportFragmentManager().findFragmentByTag("chat_user_fragment");

        Fragment chatListFragment = getSupportFragmentManager().findFragmentByTag("chat_list_fragment");
        Fragment forumMessageFragment = getSupportFragmentManager().findFragmentByTag("forum_message_fragment");
        Fragment forumMembersFragment = getSupportFragmentManager().findFragmentByTag("forum_members_fragment");

        //
        Fragment profileFragment = getSupportFragmentManager().findFragmentByTag("profile_fragment");


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if ( chatUserFragment != null) {
            removeFragment(chatUserFragment);
        }else if ( chatListFragment != null) {
            removeFragment(chatListFragment);
        }else if ( forumMembersFragment != null) {
            removeFragment(forumMembersFragment);
        }else if ( forumMessageFragment != null) {
            removeFragment(forumMessageFragment);
        }else if ( profileFragment != null) {
            removeFragment(profileFragment);
        }else {
            super.onBackPressed();
        }
    }

    private void removeFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        
        Fragment fragment = null;
        String tag = "";
        if (id == R.id.nav_profile) {
            // Handle the profile click action
            fragment = new ProfileFragment();
            tag = "profile_fragment";
        }else if (id == R.id.nav_chats) {
            fragment = new ChatListFragment();
            tag = "chat_list_fragment";
        }else if (id == R.id.nav_chats) {
            return true;
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        try{
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id._parent_frame, fragment, tag).commit();
        }catch(Exception e){
            Log.e("","Error"+e.toString());
        }
        
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * This method will be invoked when the current page is scrolled, either as part
     * of a programmatically initiated smooth scroll or a user initiated touch scroll.
     *
     * @param position             Position index of the first page currently being displayed.
     *                             Page position+1 will be visible if positionOffset is nonzero.
     * @param positionOffset       Value from [0, 1) indicating the offset from the page at position.
     * @param positionOffsetPixels Value in pixels indicating the offset from position.
     */
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    /**
     * This method will be invoked when a new page becomes selected. Animation is not
     * necessarily complete.
     *
     * @param position Position index of the new selected page.
     */
    @Override
    public void onPageSelected(int position) {
    }

    /**
     * Called when the scroll state changes. Useful for discovering when the user
     * begins dragging, when the pager is automatically settling to the current page,
     * or when it is fully stopped/idle.
     *
     * @param state The new scroll state.
     * @see ViewPager#SCROLL_STATE_IDLE
     * @see ViewPager#SCROLL_STATE_DRAGGING
     * @see ViewPager#SCROLL_STATE_SETTLING
     */
    @Override
    public void onPageScrollStateChanged(int state) {

    }
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            switch (position) {
                case 0:
                    return new LiftOffFragment();
                case 1:
                    return new HeroFragment();
                case 2:
                    return new StatsFragment();
                case 3:
                    return new ForumFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

}
