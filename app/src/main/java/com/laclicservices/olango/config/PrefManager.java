package com.laclicservices.olango.config;

import android.content.Context;
import android.content.SharedPreferences;

import com.laclicservices.olango.interfaces.IPrefManager;

/**
 * Created by Cahaksw on 23/10/2017.
 */

public class PrefManager implements IPrefManager {
    SharedPreferences mPref;
    SharedPreferences.Editor editor;
    Context _context;

    public static final String PREF_NAME="olango";
    // shared pref mode
    public static final int PRIVATE_MODE = 0;


    public PrefManager(Context context) {
        this._context = context;
        mPref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = mPref.edit();
    }

}
