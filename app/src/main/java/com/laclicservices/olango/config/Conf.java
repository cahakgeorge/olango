package com.laclicservices.olango.config;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Cahaksw on 25/10/2017.
 */

public class Conf {
    Context context;
    public static final String SMS_SENDER = "+2348069447145";
    public static final String SMS_CONDITION = "Your";

    public Conf(Context cx){
        this.context = cx;
    }

    public Typeface getFont(String name){

        Map font = new HashMap();

        font.put("medium", "fonts/avenir_medium.otf");
        font.put("black", "fonts/avenir_black.otf");
        font.put("medium_oblique", "fonts/avenir_medium_oblique.otf");
        font.put("heavy", "fonts/avenir_heavy.otf");
        font.put("rez", "fonts/rez.ttf");

        Typeface typeFace= Typeface.createFromAsset(this.context.getAssets(), (String) font.get(name));

        return typeFace;
    }

    public static String getFontName(String name){

        Map font = new HashMap();

        font.put("medium", "fonts/avenir_medium.otf");
        font.put("black", "fonts/avenir_black.otf");


        return (String) font.get(name);
    }
}
