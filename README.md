### What is this repository for? ###
This repo contains the source android native codes for the Olango prototype app. Developed for Laclic services, Lagos Nigeria.


### How do I get set up? ###
Minimim sdk version set to 17
All gradle dependencies can be found in the app's build folder

### Contribution guidelines ###
No unit tests added so far

### Who do I talk to? ###
Developed by George. Feel free to reach me by mail at cahakgeorge@gmail.com